var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var redisStore = require('connect-redis')(session);
var uuid = require('connect-uuid');



global.nodemailer = require('nodemailer');
global.mysql = require('mysql');
global.moment=require('moment');
global.redis = require('redis').createClient();
global.passport = require('passport');
global.validator = require('validator');
global.mongo = require('mongoose');
global.mongo.Ticker = require('./models/ticker.js');
global.mongo.Simbolo = require('./models/simbolo.js');
global.mongo.Quote = require('./models/quote.js');
global.passgen = require('password-generator');
global.md5 = require('md5');
global.fs = require('fs');
global.sanitize = require("sanitize-filename");
global.coins = require("bitcoin");
global.cheerio = require("cheerio"),
global.http = require("http");
global.secreto = 'arnOrnIaHeOsHefPeiryosBadyanHocekBopAjMisvavAdEigJosyagwaxocashk'; // para los hash que se hacen publicos
global.results=[];


/*global.api = { 
              product:require('./components/product.js').product, 
              cart:require('./components/cart.js').cart 
             };
*/

mongo.connect('mongodb://localhost/lira');
global.smtpTrans = global.nodemailer.createTransport({port:25,host:'localhost',secure:false,ignoreTLS:true,debug:true,
      /*auth: {
        user: '',
        pass: ''
      }*/
    });



var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(uuid());
app.use(require('express-redis')(6379, 'localhost', 'rds'));
app.use(session({
	secret: 'arnOrnIaHeOsHefPeiryosBadyanHocekBopAjMisvavAdEigJosyagwaxocashk',
	resave: true,
	saveUninitialized: true,
	cookie: { maxAge: 8640000000, secure: false },
	rolling: true,
	store: new redisStore({ host: 'localhost', port: 6379, client: redis, db:14, ttl:8640000 }),
}));
app.set('trust proxy', 1) // trust first proxy..
app.use(express.static(path.join(__dirname, 'public')));
app.use('/b',  express.static(__dirname + '/bower_components'));
app.use('/n',  express.static(__dirname + '/node_modules'));


var index = require('./controllers/index');
var api = require('./controllers/api');
var users = require('./controllers/users');

app.use('/api', api);
app.use('/', index);
app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;


setInterval(function() {
  http.get("http://localhost:"+app.get('port')+"/api/update", function(res) {
    var data = "";
    res.on('data', function (chunk) {
      data += chunk;
    });
    res.on("end", function() {
      //console.log('aca');
    });
  }).on("error", function() {
  });  
},10000);
