var express = require('express');
var router = express.Router();
var http = require('http'), csv = require('fast-csv');


// db.tickers.aggregate([{$match:{currency:'MXN',sector:{$ne:null},industria:{$ne:null}}}, {$group:{_id:{ sector: "$sector", industria:"$industria" }, avgPEG: {$avg: "$peg"}, avgPB: { $avg: "$pricebook" }, avgPE:{$avg:"$priceearnings"}, avgPS:{$avg:"$pricesales"}, avgROE:{$avg:"$roe"}, avgROA:{$avg:"$roa"}, avgCR:{$avg:"$currentratio"},avgEBITDARatio:{$avg:{$divide:["$ebitda","$revenue"]}} }}, {$sort:{sector:1,industria:1}}]).pretty()
// db.tickers.aggregate([{$match:{currency:'MXN',sector:{$ne:null},industria:{$ne:null},revenue:{$ne:0},time:{$gte:new Date('2015-09-14'),$lt:new Date("2015-09-15")}}}, {$group:{_id:{ sector: "$sector", industria:"$industria" }, avgPEG: {$avg: "$peg"}, avgPB: { $avg: "$pricebook" }, avgPE:{$avg:"$priceearnings"}, avgPS:{$avg:"$pricesales"}, avgROE:{$avg:"$roe"}, avgROA:{$avg:"$roa"}, avgCR:{$avg:"$currentratio"},avgEBITDAMargin:{$avg:{$divide:["$ebitda","$revenue"]}} }}, {$sort:{sector:1,industria:1}}]).pretty()
// db.tickers.aggregate([{$match:{currency:'MXN',sector:{$ne:null},industria:{$ne:null},time:{$gte:new Date('2015-09-14'),$lt:new Date("2015-09-15")},revenue:{$ne:0},peg:{$ne:null},pricebook:{$ne:null},pricesales:{$ne:null},priceearnings:{$ne:null},roe:{$ne:null},roa:{$ne:null}}}, {$group:{_id:{ sector: "$sector", industria:"$industria" }, avgPEG: {$avg: "$peg"}, avgPB: { $avg: "$pricebook" }, avgPE:{$avg:"$priceearnings"}, avgPS:{$avg:"$pricesales"}, avgROE:{$avg:"$roe"}, avgROA:{$avg:"$roa"}, avgCR:{$avg:"$currentratio"},avgEBITDARatio:{$avg:{$divide:["$ebitda","$revenue"]}} }}, {$sort:{sector:1,industria:1}}]).pretty()
// db.tickers.aggregate([{$match:{currency:'MXN',sector:{$ne:null},industria:{$ne:null},time:{$gte:new Date('2015-09-14'),$lt:new Date("2015-09-15")},revenue:{$ne:0},peg:{$ne:null},pricebook:{$ne:null},pricesales:{$ne:null},priceearnings:{$ne:null},roe:{$ne:null},roa:{$ne:null},currentratio:{$ne:null}}}, {$group:{_id:{ sector: "$sector", industria:"$industria" }, avgPEG: {$avg: "$peg"}, avgPB: { $avg: "$pricebook" }, avgPE:{$avg:"$priceearnings"}, avgPS:{$avg:"$pricesales"}, avgROE:{$avg:"$roe"}, avgROA:{$avg:"$roa"}, avgCR:{$avg:"$currentratio"},avgEBITDARatio:{$avg:{$divide:["$ebitda","$revenue"]}} }}, {$sort:{sector:1,industria:1}}]).pretty()
// db.tickers.aggregate([{$match:{currency:'MXN',sector:{$ne:null},industria:{$ne:null},time:{$gte:new Date('2015-09-14'),$lt:new Date("2015-09-15")},revenue:{$ne:0},peg:{$ne:null},pricebook:{$ne:null},pricesales:{$ne:null},priceearnings:{$ne:null},roe:{$ne:null},roa:{$ne:null},currentratio:{$ne:null}}}, {$group:{_id:{ sector: "$sector", industria:"$industria" }, avgPEG: {$avg: "$peg"}, avgPB: { $avg: "$pricebook" }, avgPE:{$avg:"$priceearnings"}, avgPS:{$avg:"$pricesales"}, avgROE:{$avg:"$roe"}, avgROA:{$avg:"$roa"}, avgCR:{$avg:"$currentratio"},avgEBITDARatio:{$avg:{$divide:["$ebitda","$revenue"]}},count:{$sum:1} }}, {$sort:{sector:1,industria:1}}]).pretty()
// db.tickers.aggregate([{$match:{currency:'USD',sector:{$ne:null},industria:{$ne:null},time:{$gte:new Date('2015-09-14'),$lt:new Date("2015-09-15")},revenue:{$ne:0},peg:{$ne:null},pricebook:{$ne:null},pricesales:{$ne:null},priceearnings:{$ne:null},roe:{$ne:null},roa:{$ne:null},currentratio:{$ne:null}}}, {$group:{_id:{ sector: "$sector"},                          avgPEG: {$avg: "$peg"}, avgPB: { $avg: "$pricebook" }, avgPE:{$avg:"$priceearnings"}, avgPS:{$avg:"$pricesales"}, avgROE:{$avg:"$roe"}, avgROA:{$avg:"$roa"}, avgCR:{$avg:"$currentratio"},avgEBITDARatio:{$avg:{$divide:["$ebitda","$revenue"]}},count:{$sum:1} }}, {$sort:{sector:1,industria:1}}])

// http://real-chart.finance.yahoo.com/table.csv?s=ZLTQ&a=00&b=1&c=2000&d=08&e=21&f=2015&g=d&ignore=.csv


// Utility function that downloads a URL and invokes
// callback with the data.
function download(url, callback) {
  http.get(url, function(res) {
    var data = "";
    res.on('data', function (chunk) {
      data += chunk;
    });
    res.on("end", function() {
      callback(data);
    });
  }).on("error", function() {
    callback(null);
  });
}

function numbersuffix(n)
{
  var s=n.slice(-1);
  var p=n.replace(/,/g,'');
  if(s=='T') p=p.slice(0,-1)*1000000000000;
  else if(s=='B') p=p.slice(0,-1)*1000000000;
  else if(s=='M') p=p.slice(0,-1)*1000000;
  else if(s=='K') p=p.slice(0,-1)*1000;
  else if(s=='%') p=p.slice(0,-1);
  else if(n=="N/A") p="";
  //else p=n;
  return p;
}

updateBase=function(req, res, next) {
  //http://data.okfn.org/data/core/s-and-p-500-companies
  //http://data.okfn.org/data/core/nasdaq-listings
  //http.get("http://download.finance.yahoo.com/d/quotes.csv?s=AAPL,GOOG,ALFAA.MX&f=nsl1opb4k4j5c4j4e7e8j1t8r5p6p5s6r0&e=.csv", function(response) {
  //nsl1ob4k4j5c4j4e7e8j1t8r5p6p5s6r0
  
  var simbolo, dt=new Date();
  //dt.setDate(dt.getDate()-1);
  dt.setHours(0,0,0,0);
  mongo.Simbolo.find({'$or': [{'updated':{ '$lte': dt }},{'updated':{'$eq': null }}]  }).sort({'updated':1}).limit(1).exec(function(err, model) {
  //mongo.Simbolo.find({}).sort({'updated':1}).limit(1).exec(function(err, model) {
    if(err)
    {
      //if(!res.headersSent) res.status(500).send({result:'error',message:err});
      return console.error(err);
    }
    if(model.length==0) if(!res.headersSent) res.status(200).send({result:'error',message:'No hay resultados'});
    var s='';
    for(var i=0;i<model.length;i++) {
      s=model[i]._id;
      http.get("http://download.finance.yahoo.com/d/quotes.csv?s="+s+"&f=nsl1ob4k4j5c4j4ee8j1t8r5p6p5s6r0&e=.csv", function(response) {
        //console.log("Got response: " + response.statusCode);
        var body = '';
        response.on('data', function(d) {
          body += d;
        });
        response.on('end', function() {
          csv
           .fromString(body, {headers: false})
           .on("data", function(data){
              if(data[17]=="N/A" && data[2]>0 && data[9]>0) data[17]=data[2]/data[9];
              if(data[7]=="" || data[7]==null) data[7]="USD";
              //console.log("Dato",data);
              console.log('Actualizando quote de ',data[1]);
              mongo.Ticker.update(
                  {_id:data[1]+"_"+new Date().toJSON().slice(0,10)},
                  {'$set':{_id:data[1]+"_"+new Date().toJSON().slice(0,10), 
                            name:               data[0], 
                            symbol:             data[1], 
                            last:               numbersuffix(data[2]), 
                            open:               numbersuffix(data[3]), 
                            fromhigh:           numbersuffix(data[5]), 
                            fromlow:            numbersuffix(data[6]), 
                            currency:           data[7], 
                            oneyeartarget:      numbersuffix(data[12]),
                            time:               Date.now()
                            }},
                  {safe: true, upsert: true, new : true},
                  function(err, model) {
                    //console.log('find:',model," err ",err);
                    if(err)
                    {
                      //if(!res.headersSent) res.status(500).send({result:'error',message:err});
                      return console.error(err);
                    }
                    next();
                  }
              );
              console.log("Termine de actualizar quote de ",data[1]);
           })
           .on("end", function(){             
             //if(!res.headersSent) res.status(200).send({result:'ok',message:'Updated'});
           });
        });    
      }).on('error', function(e) {
        console.log("Got error: " + e.message);
        //if(!res.headersSent) res.status(500).send({result:'error',message:e.message});
      }).end();   
    
    
    }
    //if(!res.headersSent) res.status(200).send({result:'ok',message:'Updated'});
    
  });
}

updateDetails=function(req, res, next) {
  var campos=[
                ["Book Value Per Share","bookvaluepershare"],
                ["52-Week High","high"],
                ["52-Week Low","low"],
                ["EBITDA (ttm)","ebitda"],
                ["Diluted EPS ","eps"],
                ["Market Cap","marketcap"],
                ["PEG Ratio","peg"],
                ["Price/Book","pricebook"],
                ["Price/Sales","pricesales"],
                ["Revenue Per Share ","revenue"],
                ["Trailing P/E ","priceearnings"],
                ["Return on Equity","roe"],
                ["Return on Assets","roa"],
                ["Current Ratio ","currentratio"],
                ["Total Cash (mrq)","totalcash"],
                ["Total Debt (mrq)","totaldebt"],
                ["Shares Outstanding","shares"],
                ["Avg Vol (3 month)","volume"],
                ["Trailing Annual Dividend Yield","dividend"],
                ["Qtrly Earnings Growth","qepsgrow"],
                ["Forward P/E ","epsnextyear"],
                ["Sector","sector"],
                ["Industry","industria"],
                ["Total Debt/Equity","debtequityratio"],
                ["Market Cap ","marketcap"],
              ];
  var simbolo, dt=new Date();
  dt.setHours(0,0,0,0);
  //dt.setDate(dt.getDate()-1);
  mongo.Simbolo.find({'$or': [{'updated':{ '$lte': dt }},{'updated':{'$eq': null }}]  }).sort({'updated':1}).limit(1).exec(function(err, model) {
    if(err)
    {
      //if(!res.headersSent) res.status(500).send({result:'error',message:err});
      return console.error(err);
    }
    for(var i=0;i<model.length;i++) {
      simbolo=model[i]._id;
      console.log("Actualizando metadata de ",simbolo);
      mongo.Simbolo.findByIdAndUpdate(
            simbolo,{'$set':{updated:Date.now()}}, {safe: true, upsert: true, new : false},function(err, model) {});
      
      
      var url="http://finance.yahoo.com/q/pr?s="+simbolo;
      
      
      download(url, function(data) {
        if (data) {
          //console.log(data);

          var $ = cheerio.load(data);
          for(var f=0;f<campos.length;f++) {
            var busqueda=campos[f][0], lugar=campos[f][1];
            $("TD.yfnc_tablehead1").filter(function(index) { return $(this).text().indexOf(busqueda)>=0; }).each(function(i, e) {
              //console.log($(e).text(),$(e).next().text(),numbersuffix($(e).next().text()));
              var obj={'$set':{}};
              //obj["$set"][lugar] = $(e).next().text();
              obj["$set"][lugar] = numbersuffix($(e).next().text());
              if(model[i]!=undefined) model[i][lugar]=obj["$set"][lugar];
              //console.log(obj);
              mongo.Simbolo.update(
                  {_id:simbolo},
                  obj,
                  {safe: true, upsert: true, new : true},
                  function(err, model) {
                    //console.log('find:',model," err ",err);
                    if(err)
                    {
                      //if(!res.headersSent) res.status(500).send({result:'error',message:err});
                      return console.error(err);
                    }
                  }
              );
            });
          }

          var url="http://finance.yahoo.com/q/ks?s="+simbolo+"&ql=1"; 
          download(url, function(data) {
            if (data) {
              //console.log(data);

              var $ = cheerio.load(data);
              for(var f=0;f<campos.length;f++) {
                var busqueda=campos[f][0], lugar=campos[f][1];
                $("TD.yfnc_tablehead1").filter(function(index) { return $(this).text().indexOf(busqueda)>=0; }).each(function(i, e) {
                  //console.log($(e).text(),$(e).next().text(),numbersuffix($(e).next().text()));
                  var obj={'$set':{}};
                  //obj["$set"][lugar] = $(e).next().text();
                  obj["$set"][lugar] = numbersuffix($(e).next().text());
                  if(model[i]!=undefined) obj["$set"]['sector']=model[i].sector;
                  if(model[i]!=undefined) obj["$set"]['industria']=model[i].industria;
                  //console.log(obj);
                  mongo.Ticker.update(
                      {_id:simbolo+"_"+new Date().toJSON().slice(0,10)},
                      obj,
                      {safe: true, upsert: true, new : true},
                      function(err, model) {
                        //console.log('find:',model," err ",err);
                        if(err)
                        {
                          //if(!res.headersSent) res.status(500).send({result:'error',message:err});
                          return console.error(err);
                        }
                      }
                  );
                });
              }

              console.log("Termine de actualizar metadata de",simbolo);
            }
            else console.log("error");  
          }); // Fin de detalles






          console.log("Termine de actualizar metadata de",simbolo);
        }
        else console.log("error");  
      }); // Fin de sector/industria      
      
      
      

    }
    if(!res.headersSent) res.status(200).send({result:'ok',message:'Updated',symbol:simbolo});
    
  });
}

updateQuote=function(res,req,next) {


  var dt=new Date();
  dt.setHours(0,0,0,0);
  //dt.setDate(dt.getDate()-1);
  mongo.Simbolo.find({'$or': [{'lastquoted':{ '$lt': dt }},{'lastquoted':{'$eq': null }}]  }).sort({'lastquoted':1}).limit(1).exec(function(err, model) {
    if(err)
    {
      return next(err);
    }
    var url;
    if(model.length>0) console.log("Actualizando quotes");
    for(var i=0;i<model.length;i++) {
      var m=model[i];
      if(m.lastquote==undefined) m.lastquote='1985/01/01';
      console.log("Procesando",m._id,"desde",m.lastquote.getFullYear(),m.lastquote.getMonth(),m.lastquote.getDate());
      mongo.Simbolo.findByIdAndUpdate(
            m._id,{'$set':{lastquoted:Date.now()}}, {safe: true, upsert: true, new : false},function(err, model) {});
      
      url='http://real-chart.finance.yahoo.com/table.csv?s='+m._id+'&d='+(dt.getMonth()-1)+'&e='+dt.getDate()+'&f='+dt.getFullYear()+'&g=d&a='+(m.lastquote.getMonth()-1)+'&b='+m.lastquote.getDate()+'&c='+m.lastquote.getFullYear()+'&ignore=.csv';
      download(url, function(data) {
        if (data) {
          csv
           .fromString(data, {headers: false})
           .on("data", function(data){
             if(data[0]=='Date') return;
             if(isNaN(data[1])) return;
             mongo.Quote.update({_id:{symbol:m._id,time:data[0]}},
                                {'$set':{
                                    open: data[1], 
                                    high:data[2],
                                    low:data[3],
                                    close:data[4],
                                    volume:data[5]==undefined?0:data[5]
                                  }
                                },
                                {safe: true, upsert: true, new : true},
                                function(err, model) {
                                  if(err)
                                  {
                                    console.log(err,data);
                                    return next(err);
                                  }
                                });

              mongo.Simbolo.findByIdAndUpdate(
                    m._id,{'$set':{lastquote:data[0]}}, {safe: true, upsert: true, new : false},function(err, model) {});
                                
           })
           .on("end", function(){             
             next();
           });
        }
      });
    }
  });  
}

router.get('/', function(req, res, next) {
  if(!res.headersSent) res.status(200).send({result:'ok',message:'Empty call'});
});


router.get('/ea/:currency?/:full?', function(req,res,next) {
// db.tickers.aggregate([{$match:{currency:'MXN',sector:{$ne:null},industria:{$ne:null},time:{$gte:new Date('2015-09-14'),$lt:new Date("2015-09-15")},revenue:{$ne:0},peg:{$ne:null},pricebook:{$ne:null},pricesales:{$ne:null},priceearnings:{$ne:null},roe:{$ne:null},roa:{$ne:null},currentratio:{$ne:null}}}, {$group:{_id:{ sector: "$sector", industria:"$industria" }, avgPEG: {$avg: "$peg"}, avgPB: { $avg: "$pricebook" }, avgPE:{$avg:"$priceearnings"}, avgPS:{$avg:"$pricesales"}, avgROE:{$avg:"$roe"}, avgROA:{$avg:"$roa"}, avgCR:{$avg:"$currentratio"},avgEBITDAMargin:{$avg:{$divide:["$ebitda","$revenue"]}},count:{$sum:1} }}, {$sort:{sector:1,industria:1}}]).pretty()
// db.tickers.aggregate([{$match:{currency:'USD',sector:{$ne:null},industria:{$ne:null},time:{$gte:new Date('2015-09-14'),$lt:new Date("2015-09-15")},revenue:{$ne:0},peg:{$ne:null},pricebook:{$ne:null},pricesales:{$ne:null},priceearnings:{$ne:null},roe:{$ne:null},roa:{$ne:null},currentratio:{$ne:null}}}, {$group:{_id:{ sector: "$sector"},                          avgPEG: {$avg: "$peg"}, avgPB: { $avg: "$pricebook" }, avgPE:{$avg:"$priceearnings"}, avgPS:{$avg:"$pricesales"}, avgROE:{$avg:"$roe"}, avgROA:{$avg:"$roa"}, avgCR:{$avg:"$currentratio"},avgEBITDAMargin:{$avg:{$divide:["$ebitda","$revenue"]}},count:{$sum:1} }}, {$sort:{sector:1,industria:1}}])
  var obj;
  var dtini=new Date(), dtfin=new Date();
  dtini.setDate(dtini.getDate()-1);
  if(req.params.full==1)
    obj=[{$match:{currency:req.params.currency,sector:{$ne:null},industria:{$ne:null},time:{$gte:dtini,$lt:dtfin},revenue:{$ne:0},peg:{$ne:null},pricebook:{$ne:null},pricesales:{$ne:null},priceearnings:{$ne:null},roe:{$ne:null},roa:{$ne:null},currentratio:{$ne:null}}}, {$group:{_id:{ sector: "$sector", industria:"$industria" }, avgPEG: {$avg: "$peg"}, avgPB: { $avg: "$pricebook" }, avgPE:{$avg:"$priceearnings"}, avgPS:{$avg:"$pricesales"}, avgROE:{$avg:"$roe"}, avgROA:{$avg:"$roa"}, avgCR:{$avg:"$currentratio"}, avgDER:{$avg:"$debtequityratio"},avgEBITDAMargin:{$avg:{$divide:["$ebitda","$revenue"]}}, avgPEBT:{$avg:{$divide:["$last",{$divide:["$ebitda","$shares"]}]}},count:{$sum:1} }}, {$sort:{sector:1,industria:1}}];
  else
    obj=[{$match:{currency:req.params.currency,sector:{$ne:null},industria:{$ne:null},time:{$gte:dtini,$lt:dtfin},revenue:{$ne:0},peg:{$ne:null},pricebook:{$ne:null},pricesales:{$ne:null},priceearnings:{$ne:null},roe:{$ne:null},roa:{$ne:null},currentratio:{$ne:null}}}, {$group:{_id:{ sector: "$sector" }, avgPEG: {$avg: "$peg"}, avgPB: { $avg: "$pricebook" }, avgPE:{$avg:"$priceearnings"}, avgPS:{$avg:"$pricesales"}, avgROE:{$avg:"$roe"}, avgROA:{$avg:"$roa"}, avgCR:{$avg:"$currentratio"}, avgDER:{$avg:"$debtequityratio"},avgEBITDAMargin:{$avg:{$divide:["$ebitda","$revenue"]}}, avgPEBT:{$avg:{$divide:["$last",{$divide:["$ebitda","$shares"]}]}},count:{$sum:1} }}, {$sort:{sector:1,industria:1}}];
  mongo.Ticker.aggregate(obj).exec(function(err, averages) {
    var ret=[];
    results.sort(function(a,b) { return b.rating-a.rating; });
    //console.log(results);
    console.log("Rating\tSymbol\tUltimo\tCompra\tTarget\tPotencial\tSector\tIndustria");
    for(s=0;s<results.length&&s<15;s++) {
      if(results[s].rating<0 || results[s].potencial<0) continue;
      console.log("",results[s].rating, "\t", results[s].symbol,"\t",results[s].last,"\t",results[s].purchase,"\t",results[s].target,"\t",results[s].potencial, "%\t",results[s].sector, "\t",results[s].industria, "\t");
      ret.push({type:'top',rating:results[s].rating, symbol:results[s].symbol, last: results[s].last, purchase: results[s].purchase, target: results[s].target, potencial:results[s].potencial, sector:results[s].sector, industria:results[s].industria});
    }
    results.sort(function(a,b) { return a.rating<b.rating?-1:(a.rating>b.rating?1:0); });
    for(s=0;s<results.length&&s<15;s++) {
      if(results[s].rating>0 || results[s].potencial>0) continue
      console.log("",results[s].rating, "\t", results[s].symbol,"\t",results[s].last,"\t",results[s].purchase,"\t",results[s].target,"\t",results[s].potencial, "%\t",results[s].sector, "\t",results[s].industria, "\t");
      ret.push({type:'bottom',rating:results[s].rating, symbol:results[s].symbol, last: results[s].last, purchase: results[s].purchase, target: results[s].target, potencial:results[s].potencial, sector:results[s].sector, industria:results[s].industria});
    }
    ret.sort(function(a,b) { return b.rating-a.rating; });
/*    for(s=0;s<results.length&&s<5;s++) {
      console.log("",results[(results.length-1)-s].rating, "\t", results[(results.length-1)-s].symbol,"\t",results[(results.length-1)-s].last,"\t",results[(results.length-1)-s].purchase,"\t",results[(results.length-1)-s].target,results[(results.length-1)-s].potencial, "%\t",results[(results.length-1)-s].sector, "\t",results[(results.length-1)-s].industria, "\t");
      ret.push({type:'bottom',rating:results[(results.length-1)-s].rating,
                symbol:results[(results.length-1)-s].symbol,
                last:results[(results.length-1)-s].last,
                purchase:results[(results.length-1)-s].purchase,
                target:results[(results.length-1)-s].target,
                potencial:results[(results.length-1)-s].potencial,
                sector:results[(results.length-1)-s].sector,
                industria:results[(results.length-1)-s].industria
                });
    }*/
    //if(!res.headersSent) res.status(200).send({result:'ok',message:'Recomendaciones', data: ret});
    if(!res.headersSent) res.status(200).send(ret);
    results=[];
    if(err)
    {
      if(!res.headersSent) res.status(500).send({result:'error',message:err});
      return console.error(err);
    }
    //console.log(averages);
    /* Buscar todas las acciones del currency, ponerles calificacion y regresar las 5 top y las 5 bottom
       La calificacion se calcula usando:
       * f=(PE*2)/5, tambien para PB y PS. Si esta en <=1*f se califica con -1, si esta en <=2*f con +1, si esta en <=3*f con 0, <=4*f con -1 y >=5*f -2
       * f=ROE*0.8, tambien con ROA y EBITDAMargin. Si <1*f es -1, <=2*f es +1, >=3*f es +2
       * f=CR/2, tambien PEG si <=1*f es +2, <=2*f es +1, <=3*f es 0, >=4*f es -1
    */
    for(var m=0;m<averages.length;m++) {
      var model=averages[m];
      if(req.params.full==1)
        obj={currency:req.params.currency, sector: model._id.sector, industria: model._id.industria,   time:{$gte:dtini,$lt:dtfin},revenue:{$ne:0},peg:{$ne:null},pricebook:{$ne:null},pricesales:{$ne:null},priceearnings:{$ne:null},roe:{$ne:null},roa:{$ne:null},currentratio:{$ne:null}};
      else
        obj={currency:req.params.currency, sector: model._id.sector,   time:{$gte:dtini,$lt:dtfin},revenue:{$ne:0},peg:{$ne:null},pricebook:{$ne:null},pricesales:{$ne:null},priceearnings:{$ne:null},roe:{$ne:null},roa:{$ne:null},currentratio:{$ne:null}};
      //console.log(model);
      mongo.Ticker.find(obj).exec(function(err, simbolo) {
        for(s=0;s<simbolo.length;s++) {
          var d=0,f,sm=simbolo[s], r={};
          var pricing=0, performance=0, health=0, technical=0, target=0, intrinsic;
          if(sm.last<2) continue;
          sm.pebt=sm.last/(sm.ebitda/sm.shares);
//console.log(sm.symbol,sm.peg,sm.priceearnings,model.avgPEG);
          // Parametros de valor fundamental vs precio de la accion
          f=(model.avgPE*2)/5;
          if(sm.priceearnings>=5*f) pricing-=2;
          else if(sm.priceearnings>=4*f) pricing-=1;
          else if(sm.priceearnings>=3*f) pricing+=0;
          else if(sm.priceearnings>=2*f) pricing+=1;
          else if(sm.priceearnings>=1*f) pricing+=2;
          else if(sm.priceearnings<1*f) pricing-=1;

          f=(model.avgPB*2)/5;
          if(sm.pricebook>=5*f) pricing-=2;
          else if(sm.pricebook>=4*f) pricing-=1;
          else if(sm.pricebook>=3*f) pricing+=0;
          else if(sm.pricebook>=2*f) pricing+=1;
          else if(sm.pricebook>=1*f) pricing+=2;
          else if(sm.pricebook<1*f) pricing-=1;
          
          f=(model.avgPS*2)/5;
          if(sm.pricesales>=5*f) pricing-=2;
          else if(sm.pricesales>=4*f) pricing-=1;
          else if(sm.pricesales>=3*f) pricing+=0;
          else if(sm.pricesales>=2*f) pricing+=1;
          else if(sm.pricesales>=1*f) pricing+=2;
          else if(sm.pricesales<1*f) pricing-=1;
          
          f=(model.avgPEG/2);
          if(sm.peg<=f) pricing+=2;
          else if(sm.peg<=2*f) pricing+=1;
          else if(sm.peg<=3*f) pricing+=0;
          else if(sm.peg>=4*f) pricing-=1;
          else if(sm.peg>=5*f) pricing-=2;

          f=(model.avgPEBT/2);
          if(sm.pebt<=f) pricing+=2;
          else if(sm.pebt<=2*f) pricing+=1;
          else if(sm.pebt<=3*f) pricing+=0;
          else if(sm.pebt>=4*f) pricing-=1;
          else if(sm.pebt>=5*f) pricing-=2;
          
          if(sm.last>10) pricing+=1;
          else if(sm.last>5) pricing+=0;
          else if(sm.last>=2) pricing-=1;
          else pricing-=2;


          // Parametros de calidad de la administracion
          f=(model.avgROE*0.8);
          if(sm.roe<=f) performance-=1.5;
          else if(sm.roe<=2*f) performance+=1.5;
          else if(sm.roe>=3*f) performance+=2.5;
          if(sm.roe>=15) performance+=0.75;
          if(sm.roe<10) performance-=0.75;
          
          f=(model.avgROA*0.8);
          if(sm.roa<=f) performance-=1;
          else if(sm.roa<=2*f) performance+=1;
          else if(sm.roa>=3*f) performance+=2;
          if(sm.roa>=10) performance+=0.5;
          if(sm.roa<5) performance-=0.5;
          if(sm.roa>=10 && sm.roe>=15) performance+=1;

          f=(model.avgEBITDAMargin*0.8);
          if(sm.ebitda/sm.revenue<=f) performance-=1;
          else if(sm.ebitda/sm.revenue<=2*f) performance+=1;
          else if(sm.ebitda/sm.revenue>=3*f) performance+=2;
          
          // Salud de la empresa
          f=(model.avgCR/2);
          if(sm.currentratio<=f) health-=2;
          else if(sm.currentratio<=2*f) health-=1;
          else if(sm.currentratio<=3*f) health+=0;
          else if(sm.currentratio>=4*f) health+=1;
          else if(sm.currentratio>=5*f) health+=2;
          if(sm.currentratio>=2) health+=0.5;
          if(sm.currentratio<1) health-=0.5;

          f=(model.avgDER/2);
          if(sm.debtequityratio<=f) health-=2;
          else if(sm.debtequityratio<=2*f) health-=1;
          else if(sm.debtequityratio<=3*f) health+=0;
          else if(sm.debtequityratio>=4*f) health+=1;
          else if(sm.debtequityratio>=5*f) health+=2;
          if(sm.debtequityratio>=2) health-=0.5;
          if(sm.debtequityratio<1) health+=0.5;


          // Precio segun technicals
          if(sm.high>0&&sm.low>0&&sm.last>0) technical=1-((sm.last-sm.low)/(sm.high-sm.low));
          // db.quotes.aggregate([{$match:{'_id.symbol':'AA','_id.time':{$gt:'2015-01-01'}}},{$group:{'_id':'$_id.symbol','highest':{$max:'$close'},'lowest':{$min:'$close'}}}])
          // a 1,3,5, y 10 años
          // calcular el retroceso fibonacci. Si esta entre
          // Highest-(Highest-Lowest)*0.618 y Highest-(Highest-Lowest)*0.764 aumenta la calificacion +1 por cada uno de los periodos
          
          
          
          // Calculo de precio objetivo
          target=0;
          target+=sm.last*(1+(model.avgPE-sm.priceearnings)*0.1);
          target+=sm.last*(1+(model.avgPS-sm.pricesales)*0.1);
          target+=sm.last*(1+(model.avgPB-sm.pricebook)*0.1);
          target+=sm.high+(sm.high-sm.low)*0.382;
          // Y calcula el target asi: Highest-(Highest-Lowest)*0.618+(Highest-Lowest) o lo que es lo mismo Highest+(Highest-Lowest)*0.382
          if(sm.peg>0) {
            target+=(sm.eps+sm.eps*((sm.priceearnings/(model.avgPEG-sm.peg))/500))*sm.priceearnings;
            target=Math.floor(target*100/5)/100;
          } else {
            target=Math.floor(target*100/4)/100;
          }
          
          intrinsic=sm.bookvaluepershare+((sm.roe-15)*(sm.pricebook*sm.last))/((1+15)^1);
          //purchase=Math.floor(intrinsic*0.6*100)/100;
          //purchase=Math.floor(((intrinsic*0.6+sm.high-(sm.high-sm.low)*0.618)/2)*100)/100;
          purchase=Math.floor(((sm.high-(sm.high-sm.low)*0.618))*100)/100;
          //target=Math.floor(intrinsic*0.8*100)/100;
          target=Math.floor((intrinsic*0.5+target*0.5)*100)/100;
          if(target>sm.high*1.272) target=sm.high*1.272;
          if(target<0) target=sm.low*0.382;
          target=Math.floor(target*100)/100;
          
          // Parametros para regresar y rating
          r.symbol=sm.symbol;
          r.sector=sm.sector;
          r.industria=sm.industria;
          r.target=sm.oneyeartarget>0&&sm.oneyeartarget<sm.last*2.0?Math.floor((sm.oneyeartarget*0.5+target*0.5)*100)/100:target;
          r.purchase=purchase;
          r.last=sm.last;
          r.potencial=Math.floor((r.target-r.last)/r.last*100);
          // Calculo de rating
          d=pricing*0.2+performance*0.3+health*0.25+technical*0.25+r.potencial*0.15;
          d*=100; d=Math.floor(d);
          r.rating=d;
          //console.log("",d, "\t", sm.symbol,"\t",sm.sector, "\t");
          results.push(r);
          //console.log({simbolo:sm.symbol,sector:sm.sector,calificacion:d});
        }
      });
    }
  });
});

//router.get('/updatedetails');
router.get('/update', updateQuote, updateBase, updateDetails);

module.exports = router;
