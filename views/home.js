<script type="text/x-handlebars-template" id="homeLayoutView">
  <h2>Para comprar y vender USD</h2>
  <div id="compra-venta-mxn"></div>
  <h2>Para comprar y vender MXN</h2>
  <div id="compra-venta-usd"></div>
</script>

<script type="text/x-handlebars-template" id="recomendacionView">
  <td>{{type}}</td>
  <td>{{rating}}</td>
  <td>{{symbol}}</td>
  <td>{{last}}</td>
  <td><={{purchase}}</td>
  <td>{{target}}</td>
  <td>{{potencial}}%</td>
  <td>{{sector}}</td>
  <td>{{industria}}</td>
</script>

<script>
var Home={};


Home.Layout = Marionette.LayoutView.extend({
  template: Handlebars.compile($("#homeLayoutView").html()),
  regions:{
    "compraVentaMXN":"#compra-venta-mxn",
    "compraVentaUSD":"#compra-venta-usd"
  }
});

Home.RecomendacionView = Marionette.ItemView.extend({
  tagName: 'tr',
  template: Handlebars.compile($("#recomendacionView").html())
});

Home.RecomendacionModel = Backbone.Model.extend({
  /*url: function() {
    console.log('fijando el url nuevo');
    return '/api/ea/' + this.get("currency")+'/0';
  },*/
  initialize:function(){
  }
});
Home.RecomendacionCollection = Backbone.Collection.extend({
  initialize: function(models, options) {
    this.id = options.id;
  },
  url: function() {
    return '/api/ea/' + this.id;
  },
  model: Home.RecomendacionModel,
});
Home.RecomendacionCollectionView = Marionette.CollectionView.extend({
  tagName: 'table',
  className: 'table table-hover',
  childView: Home.RecomendacionView,
});


Home.app=function(module, App, Backbone, Marionette, $, _) {

}

Home.onStart=function() {
  app.home = new Home.Layout;
  Backbone.Intercept.start();
  app.main.show(app.home);
  Backbone.history.start({pushState:true});
  Backbone.history.on("route", function(e) {
    var url = Backbone.history.root + Backbone.history.getFragment()
    //ga('send', 'pageview', url);
  });


  var recomendacionesMXN = new Home.RecomendacionCollection([],{id:'MXN/0'});
  recomendacionesMXN.fetch().then(function() {
    var recomendacionesMXNView = new Home.RecomendacionCollectionView({collection:recomendacionesMXN});
    recomendacionesMXNView.render();
    app.home.getRegion('compraVentaMXN').show(recomendacionesMXNView);
    var recomendacionesUSD = new Home.RecomendacionCollection([],{id:'USD/0'});
    recomendacionesUSD.fetch().then(function() {
      var recomendacionesUSDView = new Home.RecomendacionCollectionView({collection:recomendacionesUSD});
      recomendacionesUSDView.render();
      app.home.getRegion('compraVentaUSD').show(recomendacionesUSDView);
    });
  });


}

</script>
