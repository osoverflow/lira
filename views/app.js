var noty_timeout=2000;
var noty_timeout_fast=1500;
$(document).ajaxError(function(evt, xhr, options) {
  var responseText = xhr.responseText || 'The server did not respond on time.';
  var responseJSON = xhr.responseJSON || {message:'The server did not respond on time.'};
  var status = xhr.status || 408;
  var statusText = xhr.statusText || 'Request Timeout';

	if(status != 200){
//		var box = bootbox.alert(responseText);
//		box.find(".btn-primary").removeClass("btn-primary").addClass("btn-danger");
    noty({type:'error',force:false,dismissQueue:true,timeout:noty_timeout,layout:'top',text: responseJSON.message});
  }
  console.log('status',status,xhr);
});


var app = new Marionette.Application();

app.addRegions({
	main: '#main'
});


