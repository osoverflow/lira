var csv = require('fast-csv')
var mongo = require('mongoose');

mongo.connect('mongodb://localhost/lira');
var Schema = mongo.Schema;
var simboloSchema = new Schema({
  _id: String,
  nombre: String,
  sector: String,
  industria: String,
  updated: { type : Date, default: Date.now(), required: true }
});

var Simbolo=mongo.model('simbolo', simboloSchema);

console.log('Cargando NASDAQ');
         
csv
         .fromPath('nasdaq-listed.csv', {headers: true})
         .on("data", function(data){
           //console.log("Resultado",data,data.Symbol,data.Name,data.Sector);
           //console.log("Resultado",data.Symbol,data['Company Name'],data['Market Category']);
           Simbolo.update({_id:data.Symbol},
                          {'$set':{_id:data.Symbol, nombre: data['Company Name'] }},
                          {safe: true, upsert: true, new : true})
                          .exec(function(err,model) { 
                    //console.log('find:',model," err ",err);
                    if(err)
                    {
                      return console.error(err);
                    }
                    //console.log(model);
                    return;
            });
         })
         .on("end", function(){
            console.log('Cargando SP500');
            csv
                     .fromPath('constituents.csv', {headers: true})
                     .on("data", function(data){
                       //console.log("Resultado",data,data.Symbol,data.Name,data.Sector);
                       //console.log("Resultado",data.Symbol,data.Name,data.Sector);
                       Simbolo.update({_id:data.Symbol},
                                      {'$set':{_id:data.Symbol, nombre: data.Name, sector: data.Sector}},
                                      {safe: true, upsert: true, new : true})
                                      .exec(function(err,model) { 
                                //console.log('find:',model," err ",err);
                                if(err)
                                {
                                  return console.error(err);
                                }
                                //console.log(model);
                                return;
                        });
                     })
                     .on("end", function(){
                       console.log("done");
                       return;
                     });
         });
           
