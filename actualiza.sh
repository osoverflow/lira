#!/bin/sh
while [ 1 ]; do
	git pull --no-edit
	git push origin master
	inotifywait -e CLOSE_WRITE -r .git/logs/refs/heads/master
	sleep 1
done
