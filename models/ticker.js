mongo = require('mongoose');
var Schema = mongo.Schema;
var tickerSchema = new Schema({
  _id:String,
  name: { type: String, required: true },
  symbol: { type: String, required: true },
  last: Number,
  open: Number,
  bookvaluepershare: Number,
  fromhigh: Number,
  fromlow: Number,
  high: Number,
  low: Number,
  currency: String,
  ebitda: Number,
  eps: Number,
  epsnextyear: Number,
  marketcap: Number,
  oneyeartarget: Number,
  peg: Number,
  pricebook: Number,
  pricesales: Number,
  revenue: Number,
  priceearnings: Number,
  roe: Number,
  roa: Number,
  currentratio: Number,
  totalcash: Number,
  totaldebt: Number,
  shares: Number,
  volume: Number,
  dividend: Number,
  qepsgrow: Number,
  sector: String,
  industria: String,
  pebt: Number,
  debtequityratio: Number,
  marketcap: Number,
  time : { type : Date, default: Date.now, required: true }
});


module.exports = mongo.model('ticker', tickerSchema);
