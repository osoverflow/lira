mongo = require('mongoose');
var Schema = mongo.Schema;
var simboloSchema = new Schema({
  _id: String,
  nombre: String,
  sector: String,
  industria: String,
  updated: { type : Date, default: Date.now, required: true },
  lastquoted: { type : Date, required: false },
  lastquote: { type : Date, required: false }
});


module.exports = mongo.model('simbolo', simboloSchema);
