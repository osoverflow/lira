mongo = require('mongoose');
var Schema = mongo.Schema;
var quoteSchema = new Schema({
  _id: { 
    symbol: { type: String, required: true },
    time: { type : Date, required: true }
  },
  open: Number,
  high: Number,
  low: Number,
  close: Number,
  volume: Number,
});


module.exports = mongo.model('quote', quoteSchema);
